//
//  Alert.swift
//  eggplant-brownie
//
//  Created by Rafael Brigagão Paulino on 25/03/16.
//  Copyright © 2016 rafapaulino. All rights reserved.
//

import Foundation
import UIKit

class Alert
{
    let controller:UIViewController
    init(controller:UIViewController)
    {
        self.controller = controller
    }

    func show(title: String = "Ops!!!", message: String = "Ocorreu um erro!", buttonTitle: String = "Ok, Obrigado!")
    {
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        let ok = UIAlertAction(title: buttonTitle,style: UIAlertActionStyle.Cancel, handler: nil)
        alert.addAction(ok)
        
        controller.presentViewController(alert, animated: true, completion: nil)
    }
}