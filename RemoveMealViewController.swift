//
//  RemoveMealViewController.swift
//  eggplant-brownie
//
//  Created by Rafael Brigagão Paulino on 26/03/16.
//  Copyright © 2016 rafapaulino. All rights reserved.
//

import Foundation
import UIKit

class RemoveMealViewController {
    
    let controller:UIViewController
    
    init(controller: UIViewController) {
        self.controller = controller
    }
    
    func show(meal: Meal, handler: (UIAlertAction!) -> Void) {
        let alertController = UIAlertController(title: meal.name,
            message: meal.details(),
            preferredStyle: UIAlertControllerStyle.Alert)
        
        alertController.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
        alertController.addAction(UIAlertAction(title: "Remove", style: UIAlertActionStyle.Destructive, handler: handler))
        self.controller.presentViewController(alertController, animated: true, completion: nil)
    }
}