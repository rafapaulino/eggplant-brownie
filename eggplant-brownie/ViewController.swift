//
//  ViewController.swift
//  eggplant-brownie
//
//  Created by criare on 1/4/16.
//  Copyright © 2016 rafapaulino. All rights reserved.
//

import UIKit

protocol AddMealDelegate {
    func add(meal: Meal)
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, AddItemDelegate  {
    
    var items = Array<Item>()
    var delegate:AddMealDelegate?
    var selected = Array<Item>()

    @IBOutlet var tableView: UITableView?
    @IBOutlet var nameField: UITextField! //o ! indica que essa variavel e opcional
    @IBOutlet var happinessField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let newItemButton = UIBarButtonItem(title: "New Item",
            style: UIBarButtonItemStyle.Plain,
            target: self,
            action: Selector("showNewItem"))
        
        navigationItem.rightBarButtonItem = newItemButton
        self.items = Dao().loadItems()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //metodos para o tableview veja que ele vem sem o override
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return items.count
    }
    
    func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        let cell: UITableViewCell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: nil)
        cell.textLabel?.text = item.name
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath)
        if (cell == nil) {
            return
        }
        //colocando um marcador do tipo checkmark na table
        if (cell!.accessoryType == UITableViewCellAccessoryType.None) {
            cell!.accessoryType = UITableViewCellAccessoryType.Checkmark
            selected.append(items[indexPath.row])
        //removendo o marcador
        } else {
            cell!.accessoryType = UITableViewCellAccessoryType.None
            if let itemIndex = selected.indexOf(items[indexPath.row]) {
                selected.removeAtIndex(itemIndex)
            }
        }
    }
    
    
    
    
    
    func addNew(item: Item)
    {
        items.append(item)
        Dao().saveItems(items)
        
        if let table = tableView {
            table.reloadData()
        } else {
            //exibe a mensagem de erro com a classe customizada 
            Alert(controller: self).show("Desculpe", message: "Aconteceu um erro inesperado, mas a refeição foi adicionada", buttonTitle: "Fechar")
        }
    }
   
    @IBAction func add() {
        if let meal = getMealFromForm() {
            if let meals = delegate {
                //delega o adionar a um objeto que esta escutando essa acao
                meals.add(meal)
                
                //verifica se existe um navigation controller e entao executa a animacao
                if let navigation = self.navigationController {
                    navigation.popViewControllerAnimated(true)
                } else {
                    Alert(controller: self).show("Desculpe",
                        message: "Aconteceu um erro inesperado!",
                        buttonTitle: "Fecha logo!")
                }
            }
            return
        }
        Alert(controller: self).show("Desculpe", message: "Só dá erro aqui!", buttonTitle: "Mó brisa!")
    }
    
    func getMealFromForm() -> Meal?
    {
        if nameField == nil || happinessField == nil {
            return nil
        }
        
        let name = nameField.text!
        let happiness = Int(happinessField.text!)
        
        if happiness == nil {
            return nil
        }
        
        let meal = Meal(name: name, happiness: happiness!)
        meal.items = selected
        
        print("eaten: \(meal.name), \(meal.happiness)")
        for item in meal.items {
            print("item: \(item.name), \(item.calories)")
        }
        return meal
    }
   
    //chamando o outro controller via programacao
    func showNewItem()
    {
        let newItem = NewItemViewController(delegate: self)
        
        if let navigation = navigationController {
            navigation.pushViewController(newItem, animated: true)
        } else {
            Alert(controller: self).show()
        }
    }
    
    
    func addItem(item: Item) {
        items.append(item)
        Dao().saveItems(items)
        if let table = tableView {
            table.reloadData()
        } else {
           Alert(controller: self).show("Desculpe", message: "Aconteceu um erro inesperado, mas o item foi adicionado", buttonTitle: "Fechar")
        }
        print ("count \(items.count)")
    }

}

