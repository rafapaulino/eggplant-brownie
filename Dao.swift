//
//  Dao.swift
//  eggplant-brownie
//
//  Created by Rafael Brigagão Paulino on 26/03/16.
//  Copyright © 2016 rafapaulino. All rights reserved.
//


import Foundation

class Dao
{
    let itemsFileName = "eggplant-brownie-items.dat"
    let mealsFileName = "eggplant-brownie-meals.dat"
    
    let itemsArchive:String
    let mealsArchive:String
    
    init() {
        let directories = NSSearchPathForDirectoriesInDomains(NSSearchPathDirectory.DocumentDirectory,
            NSSearchPathDomainMask.UserDomainMask,
            true)
        let path = directories[0]
        itemsArchive = path + "/" + itemsFileName
        mealsArchive = path + "/" + mealsFileName
    }
    
    func loadItems() -> Array<Item> {
        if let items: AnyObject = NSKeyedUnarchiver.unarchiveObjectWithFile(itemsArchive) {
            return items as! Array<Item>
        } else {
            return Array<Item>()
        }
    }
    
    func saveItems(items:Array<Item>) {
        NSKeyedArchiver.archiveRootObject(items, toFile: itemsArchive)
    }
    
    func loadMeals() -> Array<Meal> {
        if let meals: AnyObject = NSKeyedUnarchiver.unarchiveObjectWithFile(mealsArchive) {
            return meals as! Array<Meal>
        } else {
            return Array<Meal>()
        }
    }
    
    func saveMeals(meals:Array<Meal>) {
        NSKeyedArchiver.archiveRootObject(meals, toFile: mealsArchive)
    }
    
}
