//: Playground - noun: a place where people can play

class Item
{
    var name:String
    var calories:Double
    
    init(name:String, calories:Double) {
        self.name = name
        self.calories = calories
    }
}

class Meal
{
    var name:String
    var happiness:Int
    var items = Array<Item>()
    
    init(name:String, happiness:Int) {
        self.name = name
        self.happiness = happiness
    }
    
    func allCalories() -> Double {
        var total = 0.0
        for i in items {
            total += i.calories
        }
        return total 
    }
}

let brownie = Meal(name: "rafael", happiness: 5)
print(brownie.name)
print(brownie.happiness)

brownie.happiness = 3
print(brownie.happiness)

let item1 = Item(name: "Caviar", calories: 50)
let item2 = Item(name: "Bolacha", calories: 150)

brownie.items.append(item1)
brownie.items.append(item2)

print(brownie.allCalories())




import UIKit

var str = "Hello, playground"

var name = "Rafael"

name = "Rafael Lindão"

print(name)

let happiness = 5

let calories = [50.5,100,200]

let eggplantBrownieIsVeggy:Bool = true

let eggplantBrownieIsChocolate:Bool = false

func helloCalorias() {
    print("Ola calorias")
}

helloCalorias()

for var i = 0; i < calories.count; i++ {
    print(calories[i])
}

for c in calories {
    print(c)
}

func allCalories(calories: Array<Double>) -> Double {
    var total = 0.0
    for c in calories {
        total += c
    }
    return total
}

let totalCalories = allCalories(calories)
print(totalCalories)



