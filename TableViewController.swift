//
//  TableViewController.swift
//  eggplant-brownie
//
//  Created by criare on 1/4/16.
//  Copyright © 2016 rafapaulino. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController, AddMealDelegate {
    
    var meals = Array<Meal>()
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section:Int) ->Int {
        return meals.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath)
    -> UITableViewCell {
        
        let row = indexPath.row
        let meal = meals[row]
        
        let cell = UITableViewCell(style: UITableViewCellStyle.Default, reuseIdentifier: nil)
        cell.textLabel?.text = meal.name
        
        let longPress = UILongPressGestureRecognizer(target: self, action: Selector("showDetails:"))
        cell.addGestureRecognizer(longPress)
        
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "addMeal") {
            let view = segue.destinationViewController as! ViewController
            view.delegate = self
        }
    }
    
    //pegando o diretorio do usuario
    override func viewDidLoad() {
        self.meals = Dao().loadMeals()
    }
    
    //adiciona uma refeicao
    func add(meal: Meal)
    {
        meals.append(meal)
        Dao().saveMeals(meals)
        tableView.reloadData()
    }
    
    func showDetails(recognizer: UILongPressGestureRecognizer)
    {
        if recognizer.state == UIGestureRecognizerState.Began {
           let cell = recognizer.view as! UITableViewCell
           let indexPath = tableView.indexPathForCell(cell)
           if indexPath == nil {
             return
           }
           let meal = meals[indexPath!.row]
                
           RemoveMealViewController(controller: self).show(meal,
             handler: { action in
                self.meals.removeAtIndex(indexPath!.row)
                Dao().saveMeals(self.meals)
                self.tableView.reloadData()
             }
           )
        }
    }
    
}

